package com.atguigu.springboot;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class Springboot03LoggingApplicationTests {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	void contextLoads() {
		//由低到高，，可以调整日志级别
		logger.trace("这是trace日志");
		logger.debug("这是debug日期");
		logger.info("这是info日期");
		logger.warn("这是warn日志");
		logger.error("这是error日志");
	}

}
