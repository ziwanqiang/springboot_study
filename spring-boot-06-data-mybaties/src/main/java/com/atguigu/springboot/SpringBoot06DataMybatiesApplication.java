package com.atguigu.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//使用MapperScan批量扫描所有Mapper接口
@MapperScan(value = "com.atguigu.springboot.mapper")
@SpringBootApplication
public class SpringBoot06DataMybatiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot06DataMybatiesApplication.class, args);
	}

}
