package com.atguigu.springboot.cotroller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ziwanqiang 2020/1/31 14:05
 */
//这个类的所有方法返回数据直接写给浏览器（如果是对象转json）
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String hello() {
        return "hello world quick!";
    }
}
